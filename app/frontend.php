<?php

namespace Wp;

use Ngearing\Wp\Loader;

class Frontend {

	public function __construct() {
		$this->loader = new Loader();

		$this->register_frontend_hooks();
	}

	private function register_frontend_hooks() {
		$this->loader->add_action( 'beforeend_main_nav', $this, 'main_nav_extra_content' );
		$this->loader->add_action( 'archive_beforeend', $this, 'archive_instagram' );
		$this->loader->add_action( 'wp_footer', $this, 'page_background' );

		$this->loader->add_filter( 'body_class', $this, 'body_class' );
		$this->loader->add_filter( 'get_the_archive_title', $this, 'archive_title' );
		$this->loader->add_filter( 'excerpt_more', $this, 'excerpt_more' );

	}

	public function main_nav_extra_content() {
		include get_theme_file_path( 'templates/section-main-menu.php' );
	}

	public function page_background() {
		$post_id = get_the_ID();
		if ( ! $post_id || ! is_page() ) {
			return;
		}

		$backgrounds = get_field( 'backgrounds', $post_id );
		if ( ! $backgrounds ) {
			$backgrounds = get_field( 'default_backgrounds', 'options' );
		}

		$image = wp_get_attachment_image( $backgrounds[ array_rand( $backgrounds ) ]['ID'], 'full' );

		printf(
			'<div class="site-background">%s</div>',
			$image
		);
	}

	public function body_class( $classes ) {
		global $post;

		if ( $post && is_object( $post ) ) {
			$classes[] = $post->post_name;
		}

		return $classes;
	}

	public function archive_instagram() {

			include get_theme_file_path( 'templates/section-instagram.php' );

	}

	public function archive_title( $title ) {
		if ( is_post_type_archive() || is_tax() ) {
			$title = get_post_type_object( get_post_type( get_the_id() ) )->label;
		} elseif ( is_singular() ) {
			$title = get_post_type_object( get_post_type( get_the_id() ) )->label;
		}

		return $title;
	}

	public function excerpt_more() {
		global $post;
		return " <a class='readmore' href='" . get_the_permalink( $post ) . "'>...read more</a>";
	}

	public function run() {
		$this->loader->run();
	}
}
