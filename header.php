<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wbs
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	
	<style>

	@media (min-width: 600px) {
		.archive .post-wrap>* {
			margin: 0 1.5em 3em;
			width: calc(50% - 3em);
    	}
    }
    
    @media (min-width: 800px) {
		.archive .post-wrap>* {
			width: calc(33.33% - 3em);
		}
	}
    
    </style> 
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wbs' ); ?></a>

	<header 
	<?php
	if ( is_admin_bar_showing() ) {
		echo 'style="top: 32px"';
	} else {
		echo 'style="top: 0;"'; }
	?>
	id="masthead" class="site-header">

		<?php if ( get_field( 'phone', 'options' ) ) : ?>
			<a class="site-phone" href="tel:<?php the_field( 'phone', 'options' ); ?>" >
				<?php the_field( 'phone', 'options' ); ?>
			</a>
		<?php endif; ?>

		<?php
		ob_start();
		require get_theme_file_path( 'dist/images/logo.svg' );
		$logo = ob_get_contents();

		ob_end_clean();

		printf(
			'<div class="site-branding">
                <%s class="site-title">
                    <a href="%s" rel="home">%s</a>
                </%1$s>
            </div>',
			is_front_page() || is_home() ? 'h1' : 'p',
			esc_url( home_url( '/' ) ),
			$logo
		);
		?>

		<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
			<span class="menu-text sr-only"><?php esc_html_e( 'Menu', 'wbs' ); ?></span>
			<i class="menu-icon fas fa-bars" aria-label="Menu Icon"></i>
		</button>

	</header><!-- #masthead -->

	<?php do_action( 'afterend_header' ); ?>

	<?php do_action( 'beforebegin_main_nav' ); ?>

	<nav id="site-navigation" class="main-navigation">

		<?php do_action( 'afterbegin_main_nav' ); ?>

		<?php
		wp_nav_menu(
			array(
				'theme_location' => 'primary',
				'menu_id'        => 'primary-menu',
				'container'      => '',
			)
		);
		?>

		<?php do_action( 'beforeend_main_nav' ); ?>

	</nav><!-- #site-navigation -->

	<?php do_action( 'afterend_main_nav' ); ?>
