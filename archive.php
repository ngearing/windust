<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wbs
 */

get_header();
?>

	<main id="main" class="site-main">

		<?php
		if ( have_posts() ) :
			?>

			<div class="post-wrap">
				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					/*
					 * Include the Post-Type-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
					 */
					get_template_part( 'templates/post', 'tile' );

				endwhile;
				?>
			</div>

			<?php
		else :

			get_template_part( 'templates/content', 'none' );

		endif;
		?>

		<?php do_action( 'archive_beforeend' ); ?>

	</main><!-- #main -->

<?php
get_footer();
