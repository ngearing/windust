;(function() {
	// document.addEventListener("DOMContentLoaded", stick)
	window.addEventListener("load", stick)

	function stick() {
		var header = document.querySelector(".site-header")
		if (header) {
			stickyElem(header)
		}

		var sidenav = document.querySelector(".single-nav")
		if (sidenav) {
			stickyElem(sidenav)
		}
	}

	function stickyElem(elem) {
		const elemHeight = elem.getBoundingClientRect().height
		const elemWidth = elem.getBoundingClientRect().width
		const offsetTop = window.scrollY + elem.getBoundingClientRect().top
		const offsetLeft = elem.getBoundingClientRect().left
		const offsetRight = document.body.clientWidth - (elemWidth - offsetLeft)

		var elemClone = elem.cloneNode(false)
		elem.parentNode.insertBefore(elemClone, elem)
		elemClone.style.height = `${elemHeight}px`
		elemClone.style.width = `${elemWidth}px`

		elem.classList.add("sticky")

		elem.style.position = "fixed"

		elem.style.height = `${elemHeight}px`
		elem.style.width = `${elemWidth}px`
		elem.style.top = `${offsetTop}px`
		elem.style.left = `${offsetLeft}px`
		// elem.style.right = `${offsetRight}px`
	}
})()
