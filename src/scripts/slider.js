require("./modules/slick.min")
;(function($) {
	$(".slider").slick({
		dots: false,
		infinite: true,
		mobileFirst: true,
		slidesToShow: 1,
		centerMode: true,
		variableWidth: true,
		responsive: [
			{
				breakpoint: 520,
				settings: {
					variableWidth: true,
					centerMode: true,
					slidesToShow: 3,
				},
			},
			{
				breakpoint: 680,
				settings: {
					variableWidth: true,
					centerMode: true,
					slidesToShow: 5,
				},
			},
		],
	})
})(jQuery)
