require("./modules/sticky.js")
require("./modules/navigation.js")
require("./modules/skip-link-focus-fix.js")
;(function() {
	"use strict"

	var scrollers = document.querySelector(".scroller")

	if (scrollers) {
		scrollers.addEventListener("click", function(event) {
			event.preventDefault()

			let href = this.getAttribute("href")
			let target = document.querySelector(href)
			target.scrollIntoView({
				block: "start",
				behavior: "smooth",
			})
		})
	}

	window.addEventListener("load", testCollision)
	window.addEventListener("scroll", testCollision)

	function testCollision() {
		var header = document.querySelectorAll(".site-header")[1]
		var content = document.querySelector(".page-content")
		var siteMain = document.querySelector(".site-main")
		var slider = document.querySelector(".slider")
		var footer = document.querySelector(".site-footer")

		if (
			(content && doElsCollide(header, content)) ||
			(siteMain && doElsCollide(header, siteMain)) ||
			(slider && doElsCollide(header, slider)) ||
			(footer && doElsCollide(header, footer))
		) {
			header.classList.add("black")
		} else {
			if (header.classList.contains("black")) {
				header.classList.remove("black")
			}
		}
	}

	// Detect Collison
	var doElsCollide = function(el1, el2) {
		return !(
			el1.getBoundingClientRect().bottom < el2.getBoundingClientRect().top ||
			el1.getBoundingClientRect().top > el2.getBoundingClientRect().bottom ||
			el1.getBoundingClientRect().right < el2.getBoundingClientRect().left ||
			el1.getBoundingClientRect().left > el2.getBoundingClientRect().right
		)
	}
})()
