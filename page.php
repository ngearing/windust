<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wbs
 */

get_header();
?>

	<main id="main" class="site-main">

		<header class="page-header">
			<div class="page-intro">
				<?php the_field( 'intro' ); ?>

				<?php
				if ( get_field( 'show_scroller' ) ) :
					?>
					<a href="#content" class="scroller">
						<i class="fas fa-chevron-down"></i>
					</a>
				<?php endif; ?>
			</div>
		</header><!-- .page-header -->

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'templates/content', 'page' );

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->

		<div class="slider">
			<?php
			if ( have_rows( 'images' ) && get_field( 'enable' ) ) {
				// Load slider assets.
				wp_enqueue_style( 'slider-css' );
				wp_enqueue_script( 'slider-js' );

				while ( have_rows( 'images' ) ) {
					the_row();

					$image = get_sub_field( 'image' );
					if ( ! empty( $image ) ) {
						?>
						<div class="slide">
							<?php echo wp_get_attachment_image( $image['id'], 'medium' ); ?>
						</div>
						<?php
					}
				}
			}
			?>
		</div>

<?php
get_footer();
