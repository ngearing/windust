<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wbs
 */

?>

	<?php do_action( 'beforebegin_footer' ); ?>

	<footer id="colophon" class="site-footer">

		<?php do_action( 'afterbegin_footer' ); ?>

		<div class="site-info">
			<p><?php bloginfo(); ?></p>
			<?php require get_theme_file_path( 'templates/section-social-links.php' ); ?>
		</div><!-- .site-info -->

		<div class="site-credits">
			<p>Website by <a href="https://greengraphics.com.au" target="_blank">Greengraphics</a></p>
		</div>

		<?php do_action( 'beforeend_footer' ); ?>

	</footer><!-- #colophon -->

	<?php do_action( 'afterend_footer' ); ?>

	<?php wp_footer(); ?>
</div><!-- #page -->

</body>
</html>
