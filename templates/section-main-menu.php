<div class="menu-content">

	<p class="site-title"><?php bloginfo(); ?> <?php bloginfo( 'description' ); ?></p>
	<p class="site-email"><a href="mailto:&#073;&#078;&#070;&#079;&#064;&#087;&#073;&#078;&#068;&#085;&#083;&#084;&#046;&#067;&#079;&#077;&#046;&#065;&#085;">&#073;&#078;&#070;&#079;&#064;&#087;&#073;&#078;&#068;&#085;&#083;&#084;&#046;&#067;&#079;&#077;&#046;&#065;&#085;</a></p>
	<p class="site-phone"><a href="tel:<?php echo get_field( 'phone', 'options' ); ?>"><?php echo get_field( 'phone', 'options' ); ?></a></p>
	<p class="site-address"><a href="https://www.google.com/maps/place/WINDUST+ARCHITECTURE+x+INTERIORS/@-37.8344543,144.9569741,17z/data=!3m1!4b1!4m5!3m4!1s0x6ad667fe9645960d:0x1de9bc1d37ed55d!8m2!3d-37.8344586!4d144.9591628" target="_blank"><?php echo get_field( 'address', 'options' ); ?></a></p>

	<?php require get_theme_file_path( 'templates/section-social-links.php' ); ?>

</div>
