<nav class="single-nav">
	<ul class="menu">
		<li><a href="<?php echo esc_url( home_url( '/project' ) ); ?>" title="go back to projects"><i class="fas fa-long-arrow-alt-left"></i> Projects</a></li>
	</ul>
</nav>
