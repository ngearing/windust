<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wbs
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'post-tile' ); ?>>

	<header class="post-header">
		<?php
			the_title( '<h1 class="post-title">', '</h1>' );
		?>
	</header>

	<div class="post-content">
		<?php the_content(); ?>
	</div>

</article>
