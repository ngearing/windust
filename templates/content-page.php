<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wbs
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="page-content" id="content">
		<?php
		the_content();
		?>
	</div><!-- .page-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="page-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'wbs' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .page-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->

<?php
/*
 * Hide this for the moment.
 *
 * if ( get_field( 'message' ) ) {
<div class="testimonial">
	<p class="testimonial-text">Our client's say...</p>
	<?php the_field( 'message' ); ?>
	<?php if ( get_field( 'name' ) ) { ?>
		<p class="testimonial-name"><?php the_field( 'name' ); ?></p>
	<?php } ?>
	<?php if ( get_field( 'company' ) ) { ?>
		<p class="testimonial-company"><?php the_field( 'company' ); ?></p>
	<?php } ?>
</div>
<?php } ?>
*/
