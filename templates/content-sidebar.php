<?php
$fields = get_fields();

foreach ( $fields as $key => $field ) {
	$field_template = "templates/field-$key.php";
	if ( file_exists( get_theme_file_path( $field_template ) ) ) {
		include get_theme_file_path( $field_template );
	}
}
