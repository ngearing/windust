<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wbs
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="news-left">
		<header class="page-header">
			<h2 class="page-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<p class="posted-on"><?php echo get_the_date(); ?></p>
		</header><!-- .page-header -->

		<div class="page-content">
			<?php the_content(); ?>
		</div><!-- .page-content -->
	</div>
	<div class="news-right">
		<?php echo wp_get_attachment_image( get_field( 'featured_image' )['id'], 'full' ); ?>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
