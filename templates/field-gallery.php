<?php
$gallery = get_field( 'gallery' );
if ( ! $gallery ) {
	return;
}
?>

<section class="gallery">
	<?php foreach ( $gallery as $image ) : ?>

		<div class="gallery-item">
			<?php echo wp_get_attachment_image( $image['ID'], 'full' ); ?>
		</div>

	<?php endforeach; ?>
</section>
