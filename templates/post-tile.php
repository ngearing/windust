<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wbs
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'post-tile' ); ?>>

<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">

	<div class="post-thumbnail">
		<?php echo wp_get_attachment_image( get_post_thumbnail_id( get_the_id() ), 'post-tile' ); ?>
	</div>

	<header class="post-header">
		<?php
			the_title( '<h2 class="post-title">', '</h2>' );
		?>
	</header>

	</a>

</article>
