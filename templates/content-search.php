<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wbs
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="page-header">
		<?php the_title( sprintf( '<h2 class="page-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .page-header -->

	<?php wbs_post_thumbnail(); ?>

	<div class="page-summary">
		<?php the_excerpt(); ?>
	</div><!-- .page-summary -->

</article><!-- #post-<?php the_ID(); ?> -->
