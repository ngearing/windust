<?php

namespace Wp;

class Acf {
	public function __construct() {
		$fields_path = get_stylesheet_directory() . '/lib/acf-fields';

		if ( ! file_exists( $fields_path ) ) {
			mkdir( $fields_path, 0755 );
		}

		$this->fields_path = $fields_path;

		add_filter( 'acf/settings/save_json', [ $this, 'save_json' ] );
		add_filter( 'acf/settings/load_json', [ $this, 'load_json' ] );

		add_action( 'init', [ $this, 'add_options_page' ] );
	}

	public function save_json( $path ) {
		return $this->fields_path;
	}

	public function load_json( $paths ) {
		$paths[] = $this->fields_path;
		return $paths;
	}

	public function add_options_page() {
		acf_add_options_page( 'Theme Options' );
	}
}
