<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package wbs
 */

get_header();
?>

	<main id="main" class="site-main">

		<?php require get_theme_file_path( 'templates/nav-single.php' ); ?>

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'templates/content', get_post_type() );

		endwhile; // End of the loop.
		?>

		<?php require get_theme_file_path( 'templates/field-gallery.php' ); ?>

		<?php
		the_post_navigation(
			[
				'prev_text' => "<i class='fas fa-long-arrow-alt-left'></i> Previous",
				'next_text' => "Next <i class='fas fa-long-arrow-alt-right'></i>",
			]
		);
		?>

	</main><!-- #main -->

<?php
get_footer();
