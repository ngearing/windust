<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Ngearing\\Wp\\AdminPage' => $vendorDir . '/ngearing/wp/src/AdminPage.php',
    'Ngearing\\Wp\\Loader' => $vendorDir . '/ngearing/wp/src/Loader.php',
    'Ngearing\\Wp\\PostType' => $vendorDir . '/ngearing/wp/src/PostType.php',
    'Wp\\Acf' => $baseDir . '/lib/acf.php',
    'Wp\\Cleanup' => $baseDir . '/lib/cleanup.php',
    'Wp\\Frontend' => $baseDir . '/app/frontend.php',
    'Wp\\PostTax' => $baseDir . '/lib/post-tax.php',
    'Wp\\Theme' => $baseDir . '/lib/theme.php',
);
